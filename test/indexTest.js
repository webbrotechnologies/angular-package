describe('Index Test Suit',function(){
    var $rootScope,$scope,$controller,mainCntrl;
    beforeEach(module('myApp'));
    beforeEach(inject(function(_$rootScope_,$controller){
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        mainCntrl = $controller('MainCntrl',{'$rootScope' : $rootScope, '$scope': $scope})
    }))
    it('should it check controller Exisit',function(){
        var hay = 'Hello';
        expect(mainCntrl).toBeDefined();
        expect($scope.welcome).toBe('Welcome Please')
    });

});