var gulp = require('gulp');
var connect = require('gulp-connect');
var karma = require('gulp-karma');



// Start the server
gulp.task('start', function () {
    connect.server({
        livereload: true
    });
})

// Reload the page automatically

gulp.task('Refresh', function () {
    return gulp.src('./app/*.html')
        .pipe(connect.reload())
})

// Watch for change

gulp.task('Watch',function(){
    gulp.watch(['./app/*.html'],['Refresh'])
})

gulp.task('unit',function(){
    return gulp.src('./app/test/*Test.js')
        .pipe(karma({
            configFile:'karma.config.js',
            action:'run'
        })).on('error',function(err){
            throw err;
        })
})

gulp.task('Run',['start','Watch']);
